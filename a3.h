// a3.h

/////////////////////////////////////////////////////////////////////////
//
// Student Info
// ------------
//
// Name : <put your full name here!>
// St.# : <put your full SFU student number here>
// Email: <put your SFU email address here>
//
//
// Statement of Originality
// ------------------------
//
// All the code and comments below are my own original work. For any non-
// original work, I have provided citations in the comments with enough
// detail so that someone can see the exact source and extent of the
// borrowed work.
//
// In addition, I have not shared this work with anyone else, and I have
// not seen solutions from other students, tutors, websites, books,
// etc.
//
/////////////////////////////////////////////////////////////////////////


#include "cmpt_error.h" // These are the only
#include <iostream>     // files you can #include. 
#include <fstream>      // Don't remove or change
#include <string>       // any of them, and don't 
#include <algorithm>    // #include any others.
#include <cassert>      

using namespace std;

class int_vec {
private:
	// ...

public:
	// ...

}; // class int_vec

// ...
