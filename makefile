####################################################################
#
# Makefile for CMPT 135 Spring 2021, SFU Surrey.
#
#####################################################################

# Set the C++ compiler options:
#   -std=c++17 compiles using the C++17 standard (or at least as 
#    much as is implemented by the compiler, e.g. for g++ see
#    http://gcc.gnu.org/projects/cxx0x.html)
#   -Wall turns on all warnings
#   -Wextra turns on even more warnings
#   -Werror causes warnings to be errors 
#   -Wfatal-errors stops the compiler after the first error
#   -Wno-sign-compare turns off warnings for comparing signed and 
#    unsigned numbers
#   -Wnon-virtual-dtor warns about non-virtual destructors
#   -g puts debugging info into the executables (makes them larger)
CPPFLAGS = -std=c++17 -Wall -Wextra -Werror -Wfatal-errors -Wno-sign-compare -Wnon-virtual-dtor -g

GRAPH = a3_graph

$(GRAPH): $(GRAPH).o
	g++ $(GRAPH).o -o $(GRAPH) -lsfml-graphics -lsfml-window -lsfml-system

$(GRAPH).o: $(GRAPH).cpp
	g++ $(CPPFLAGS) -c $(GRAPH).cpp

submit:
	rm -f a3_submit.zip
	zip a3_submit.zip a3.h a3_test.cpp a3_graph.cpp

clean:
	rm -f a3_test
	rm -f $(GRAPH) $(GRAPH).o