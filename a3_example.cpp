// a3_example.cpp

#include "a3_sol.h"
#include <string>

using namespace std;

double average(int_vec v) {
    int total = 0;
    for (int i = 0; i < v.size(); i++) {
        total += v.get(i);
    }
    return double(total) / v.size();
}

int main() {
    int_vec a;    // { }
    a.append(2);  // {2}
    a.append(5);  // {2, 5}
    a.prepend(2); // {2, 2, 5}
    a.prepend(3); // {3, 2, 2, 5}
    a.println();

    int_vec b(5, 0); // {0, 0, 0, 0, 0}
    b.println();

    if (a == b) {
        cout << "a and b are the same!\n";
    } else {
        cout << "a and b are different!\n";
    }
    
    cout << "average of a: " << average(a) << "\n";
    cout << "average of b: " << average(b) << "\n";

    a.sort();
    a.reverse();
    a.remove_all(2);
    a.println();
}
